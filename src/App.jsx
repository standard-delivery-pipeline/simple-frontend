import React, { useCallback, useState } from 'react';
import './App.css';

function getDataFromServer() {
  return fetch(process.env.REACT_APP_HOST + `/${process.env.REACT_APP_API_BASE_PATH}/hello`).then((response) =>
    response.text()
  );
}

function App() {
  const [text, setText] = useState('');
  const [textColor, setTextColor] = useState('white-text');

  const handleButtonPress = useCallback((style) => {
    getDataFromServer().then((data) => {
      setTextColor(style + '-text');
      setText(data.toString());
    });
  }, []);

  return (
    <div className="app">
      <p className={textColor}>{text}</p>
      <div className="button-container">
        <button className="button red" onClick={() => handleButtonPress('red')}>
          Red request button
        </button>
        <button className="button green" onClick={() => handleButtonPress('green')}>
          Green request button
        </button>
        <button className="button blue" onClick={() => handleButtonPress('blue')}>
          Blue request button
        </button>
      </div>
    </div>
  );
}

export default App;
