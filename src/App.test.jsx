import { act, fireEvent, render } from '@testing-library/react';
import App from './App';

describe('App tests', () => {
  let tests;
  const mockResponse = 'Dummy';

  beforeEach(() => {
    jest.spyOn(global, 'fetch').mockResolvedValue({
      text: jest.fn().mockResolvedValue(mockResponse),
    });

    tests = render(<App />);
  });

  it('should call fetch and render text with button color', async () => {
    const redButton = tests.getByText('Red request button');

    await act(async () => fireEvent.click(redButton));

    const renderedText = tests.getByText(mockResponse);

    expect(renderedText.className).toBe('red-text');
  });
});
